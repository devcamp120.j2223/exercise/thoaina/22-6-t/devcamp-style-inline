import avatar from "./assets/images/48.jpg";

const style = {
  devcampContainer: {
    textAlign: "center",
    margin: "100px auto",
    width: "800px",
    border: "1px solid #cacaca",
    backgroundColor: "bisque"
  },
  devcampAvatar: {
    marginTop: "-60px",
    borderRadius: "50%",
  },
  devcampIntro: {
    fontSize: "12px",
    color: "brown"
  },
  devcampName: {
    fontWeight: "bold",
    color: "blue"
  }
}

function App() {
  return (
    <div style={style.devcampContainer}>
      <img src={avatar} style={style.devcampAvatar} />
      <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      <p style={style.devcampIntro}><span style={style.devcampName}>Tammy Stevens</span> . Front End Developer</p>
    </div>
  );
}

export default App;
